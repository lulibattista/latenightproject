using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField] private Transform Player;
    private Vector3 Direcction;
    private Rigidbody rb;
    public float SpeedMouse = 50f;
    [SerializeField] private float maxY;
    [SerializeField] private float rX;
    [SerializeField] private Transform CamRotation;
    [SerializeField] private Transform Cam;
    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        Direcction = Player.TransformVector(new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized);
        rX = Mathf.Lerp(rX, Input.GetAxisRaw("Mouse X") * 2, 100 * Time.deltaTime);
        maxY = Mathf.Clamp(maxY - (Input.GetAxisRaw("Mouse Y") * 2 * 100 * Time.deltaTime), -30, 30f);
        Player.Rotate(0, rX, 0, Space.World);
        Cam.rotation = Quaternion.Lerp(Cam.rotation, Quaternion.Euler(maxY * 2, Player.eulerAngles.y, 0), 100 * Time.deltaTime);
        //CamRotation.position = Vector3.Lerp(CamRotation.position, Player.position, 10 * Time.deltaTime);

    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + Direcction * 10 * Time.fixedDeltaTime);

    }

    

}
