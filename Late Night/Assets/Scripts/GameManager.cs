using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool Loser;
    public bool Winner;
    public GameObject GameoverText;
    public GameObject GameWinnerText;
    public MovementControl playerMovement;

    void Start()
    {
        Loser = false;
        Winner = false;
        GameoverText.SetActive(false);
        playerMovement.AllowMovement();
       
    }

    void Update()
    {

    }

    public void GameOver()
    {
        
        Loser = true;
        GameoverText.SetActive(true);
        StopPlayerMovement();
    }

    public void GameWinner()
    {
        Winner = true;
        GameWinnerText.SetActive(true);
        StopPlayerMovement();
    }

    void StopPlayerMovement()
    {
        if (playerMovement != null)
        {
            playerMovement.StopMovement();
        }
        else
        {
            Debug.LogError("PlayerMovement no est� asignado en GameManager.");
        }
    }

}
