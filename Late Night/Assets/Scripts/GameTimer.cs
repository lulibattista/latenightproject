using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameTimer : MonoBehaviour
{
    public GameTimer instance;
    public static float TiempoOriginal;
    public static float TiempoPasado;
    public static float TiempoRestante;
    public static int Minutos;
    public GameManager ManagerGame;
    public static int CantidadVeces=1;
    public TMP_Text TimeramText;
    private bool ejecutarTemporizador = true;
    public static bool primeraVez = true;
    public static bool GaneAhorcado = false;
    public static bool GaneMemotest = false;
    public static bool GanePuzzle9 = false;
    


    void Start()
    {
        if (primeraVez)
        {
            TiempoOriginal = 20000;
            TiempoPasado = 0;
            TiempoRestante = TiempoOriginal;
            TimeramText.text = CantidadVeces + " AM";
            if (ManagerGame == null)
            {
                Debug.LogError("GameManager no est� asignado en SanityControl.");
            }
            primeraVez = false;
        }
        TimeramText.text = CantidadVeces + " AM";
    }

    
    void Update()
    {
        Minutos = Mathf.FloorToInt((TiempoPasado / TiempoOriginal) * 60);
        if (Minutos < 10)
        {
            TimeramText.text = CantidadVeces + ":0" + Minutos + " AM";
        }
        else
        {
            TimeramText.text = CantidadVeces + ":" + Minutos + " AM";
        }
        
        

        if (GaneAhorcado)
        {
            GaneAhorcado = false;
            CantidadVeces += 1;
        }
        if (GaneMemotest)
        {
            GaneMemotest = false;
            CantidadVeces += 1;
        }
        if (GanePuzzle9)
        {
            GanePuzzle9 = false;
            CantidadVeces += 2;
        }

        if (TiempoRestante <= 0)
        {
            if(CantidadVeces < 5)
            {
                TiempoOriginal = 20000;
                TiempoPasado = 0;
                TiempoRestante = TiempoOriginal;
                CantidadVeces += 1;
                IniciaTemporizador();
            }
            else if (ManagerGame != null)
            {
                ManagerGame.GameWinner();
                TimeramText.text = "6:00 AM";
            }
        }
        else
        {
            IniciaTemporizador();
        }
    }

    private IEnumerator Esperar()
    {
        int tiempo_espera = 20;

        while (tiempo_espera > 0)
        {

            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
    }
    public void IniciaTemporizador()
    {
        if (ejecutarTemporizador)
        {
            StartCoroutine("Esperar");
            if (TiempoPasado <= TiempoOriginal)
            {
                StartCoroutine("Esperar");
                TiempoPasado += 1;
                TiempoRestante = TiempoOriginal - TiempoPasado;
                
            }
        }
        
    }
    public void DetenerTemporizador()
    {
        ejecutarTemporizador = false;
        
    }
}
