using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;

public class HandlerImagen : MonoBehaviour, IPointerClickHandler
{

    public MemoControl memoControl;

    public void OnPointerClick(PointerEventData eventData)
    {
        int TarjetaElejidaIndice;
        string TarjetaElejidaNombre;
        Color TarjetaElegidaColor;


        TarjetaElejidaNombre = gameObject.name;
        TarjetaElejidaIndice = BuscoIndiceTarjeta(TarjetaElejidaNombre);



        if (memoControl.Llamada == 2)
        {
            memoControl.Llamada = 0;
            if (memoControl.TarjetasDisponibles > 0)
            {
                PintamosNegro();
            }
        }
        else
        {
            Image imagen = gameObject.GetComponent<Image>();
            //imagen.color = TarjetaElegidaColor;
            imagen.color = Color.white;

            if (memoControl.Llamada == 0)
            {
                memoControl.Llamada = 1;
                memoControl.PrimerTarjeta = gameObject;
                memoControl.IndicePrimeraElejida = TarjetaElejidaIndice;
            }
            else
            {
                if (memoControl.Llamada == 1)
                {
                    memoControl.Llamada = 2;
                    memoControl.SegundaTarjeta = gameObject;
                    memoControl.IndiceSegundaElegida = TarjetaElejidaIndice;
                    if (HayCoincidencia(memoControl.IndicePrimeraElejida, memoControl.IndiceSegundaElegida))
                    {
                        Destroy(memoControl.PrimerTarjeta, 1f);
                        Destroy(memoControl.SegundaTarjeta, 1f);
                        memoControl.Llamada = 0;
                        memoControl.TarjetasDisponibles = memoControl.TarjetasDisponibles - 2;
                    }
                    else
                    {
                        memoControl.CantidadIntentos += 1;
                    }
                }
            }
        }
    }

    int BuscoIndiceTarjeta(string Tarjeta)
    {
        int indice;
        switch (Tarjeta)
        {
            case "Image_1":
                indice = 0;
                break;
            case "Image_2":
                indice = 1;
                break;
            case "Image_3":
                indice = 2;
                break;
            case "Image_4":
                indice = 3;
                break;
            case "Image_5":
                indice = 4;
                break;
            case "Image_6":
                indice = 5;
                break;
            case "Image_7":
                indice = 6;
                break;
            case "Image_8":
                indice = 7;
                break;
            case "Image_9":
                indice = 8;
                break;
            case "Image_10":
                indice = 9;
                break;
            case "Image_11":
                indice = 10;
                break;
            case "Image_12":
                indice = 11;
                break;
            case "Image_13":
                indice = 12;
                break;
            case "Image_14":
                indice = 13;
                break;
            case "Image_15":
                indice = 14;
                break;
            case "Image_16":
                indice = 15;
                break;
            default:
                indice = -1;
                break;
        }
        return indice;
    }


    void PintamosNegro()
    {
        Image imagen1 = memoControl.PrimerTarjeta.GetComponent<Image>();
        imagen1.color = Color.black;

        Image imagen2 = memoControl.SegundaTarjeta.GetComponent<Image>();
        imagen2.color = Color.black;
    }

    bool HayCoincidencia(int indice1, int indice2)
    {
        if (memoControl.listaDeTarjetas[indice1] == memoControl.listaDeTarjetas[indice2])
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator EsperarAntesDeContinuar(int tiempo)
    {
        yield return new WaitForSeconds(tiempo); // Espera 3 segundos

    }
}
