using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class HandlerTecla : MonoBehaviour, IPointerClickHandler
{
    public Puzzle9Control scriptPpal;


    public void OnPointerClick(PointerEventData eventData)
    {
        int TarjetaNumeralPos;
        int TarjetaElegidaPos;
        GameObject TarjetaElegida;
        GameObject TarjetaNumeral;
        Image ImagenElegida;
        Image ImagenNumeral;
        Sprite spriteaux;

        Debug.Log("Entro en click");

        TarjetaNumeralPos = scriptPpal.PosicionNumeral;
        TarjetaElegida = gameObject;
        TarjetaElegidaPos = BuscoPosTarjeta(TarjetaElegida.name);

        if (PuedoMover(TarjetaElegidaPos, TarjetaNumeralPos))
        {
            ImagenElegida = TarjetaElegida.GetComponent<Image>();

            TarjetaNumeral = BuscoTarjeta(TarjetaNumeralPos);
            ImagenNumeral = TarjetaNumeral.GetComponent<Image>();

            spriteaux = ImagenElegida.sprite;
            ImagenElegida.sprite = ImagenNumeral.sprite;
            ImagenNumeral.sprite = spriteaux;

            scriptPpal.PosicionesNumeros[TarjetaNumeralPos - 1] = scriptPpal.PosicionesNumeros[TarjetaElegidaPos - 1];
            scriptPpal.PosicionesNumeros[TarjetaElegidaPos - 1] = 9;

            scriptPpal.PosicionNumeral = TarjetaElegidaPos;
        }
    }

    bool PuedoMover(int poselegida, int posnumeral)
    {
        bool respuesta = false;
        switch (posnumeral)
        {
            case 1:
                if (poselegida == 2 || poselegida == 4)
                {
                    respuesta = true;
                }
                break;
            case 2:
                if (poselegida == 1 || poselegida == 3 || poselegida == 5)
                {
                    respuesta = true;
                }
                break;
            case 3:
                if (poselegida == 2 || poselegida == 6)
                {
                    respuesta = true;
                }
                break;
            case 4:
                if (poselegida == 1 || poselegida == 5 || poselegida == 7)
                {
                    respuesta = true;
                }
                break;
            case 5:
                if (poselegida == 2 || poselegida == 4 || poselegida == 8 || poselegida == 6)
                {
                    respuesta = true;
                }
                break;
            case 6:
                if (poselegida == 3 || poselegida == 5 || poselegida == 9)
                {
                    respuesta = true;
                }
                break;
            case 7:
                if (poselegida == 4 || poselegida == 8)
                {
                    respuesta = true;
                }
                break;
            case 8:
                if (poselegida == 7 || poselegida == 5 || poselegida == 9)
                {
                    respuesta = true;
                }
                break;
            case 9:
                if (poselegida == 8 || poselegida == 6)
                {
                    respuesta = true;
                }
                break;
        }

        return respuesta;
    }

    int BuscoPosTarjeta(string Tarjeta)
    {
        int indice;
        switch (Tarjeta)
        {
            case "Image_1":
                indice = 1;
                break;
            case "Image_2":
                indice = 2;
                break;
            case "Image_3":
                indice = 3;
                break;
            case "Image_4":
                indice = 4;
                break;
            case "Image_5":
                indice = 5;
                break;
            case "Image_6":
                indice = 6;
                break;
            case "Image_7":
                indice = 7;
                break;
            case "Image_8":
                indice = 8;
                break;
            case "Image_9":
                indice = 9;
                break;
            default:
                indice = -1;
                break;
        }
        return indice;
    }


    GameObject BuscoTarjeta(int posicion)
    {
        string nombreobjeto;
        GameObject objeto;

        switch (posicion)
        {
            case 1:
                nombreobjeto = "Image_1";
                break;
            case 2:
                nombreobjeto = "Image_2";
                break;
            case 3:
                nombreobjeto = "Image_3";
                break;
            case 4:
                nombreobjeto = "Image_4";
                break;
            case 5:
                nombreobjeto = "Image_5";
                break;
            case 6:
                nombreobjeto = "Image_6";
                break;
            case 7:
                nombreobjeto = "Image_7";
                break;
            case 8:
                nombreobjeto = "Image_8";
                break;
            case 9:
                nombreobjeto = "Image_9";
                break;
            default:
                nombreobjeto = "Image_9";
                break;
        }
        objeto = GameObject.Find(nombreobjeto);
        return objeto;
    }
}

