using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HangedManControl : MonoBehaviour
{

    
    public TMP_Text txtPalabraMostrar;
    public TMP_Text txtIntentos;
    public TMP_Text txtResultados;
    public string LaPalabra;
    public string PalabraMostrar;
    public GameObject ImagenAhorcado;
    public int CantidadIntentos;
    public bool GanoMiniAhorcado;
    public int CantidadLetrasCorrectas;
    public Sprite spriteaux;
    public GameObject objaux;
    public Image imagenaux;
    public GameObject ImagenWinner;
    public GameObject ImagenLoser;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        ImagenWinner.SetActive(false);
        string[] misPalabras = new string[12];
        int NumeAleatorio = Random.Range(0, 11);

        misPalabras[0] = "EDIFICIO";
        misPalabras[1] = "SOLUCION";
        misPalabras[2] = "CONFESAR";
        misPalabras[3] = "EDUCADOR";
        misPalabras[4] = "DIVISION";
        misPalabras[5] = "ABSORBER";
        misPalabras[6] = "PREVENIR";
        misPalabras[7] = "VESTIDOR";
        misPalabras[8] = "MILLONES";
        misPalabras[9] = "ETIQUETA";
        misPalabras[10] = "PRESENTA";
        misPalabras[11] = "RECUERDA";
        LaPalabra = misPalabras[NumeAleatorio];

        PalabraMostrar = "--------";
        txtPalabraMostrar.text = PalabraMostrar;
        CantidadIntentos = 0;
    }

    // Update is called once per frame
    void Update()
    {

        
        string Ingreso = Input.inputString;

        if (!string.IsNullOrEmpty(Ingreso))
        {
            char letra = char.ToUpper(Ingreso[0]);
           
            txtIntentos.text = "Intentos = " + CantidadIntentos;

            if (BuscarLaLetra(letra))
            {
                ReemplazarLetra(letra);
                txtPalabraMostrar.text = PalabraMostrar;
            }
            else
            {
                CantidadIntentos++;
                objaux = BuscoImagen(CantidadIntentos);
                imagenaux = objaux.GetComponent<Image>();
                spriteaux = imagenaux.sprite;

                ImagenAhorcado.GetComponent<Image>().sprite = spriteaux;
                
                //Aca hay que cargar la imagen del ahorcado con una parte del cuerpo agregada. 
            }
        }
        if(CantidadLetrasCorrectas == 8)
        {
            GanoMiniAhorcado = true;
            txtResultados.text = "G A N A S T E";
            Debug.Log("Gano mini juego");
            GameTimer.GaneAhorcado = true;
            ImagenWinner.SetActive(true);
            audioSource.Stop();

        }
        else if (CantidadIntentos==8)
        {
                GanoMiniAhorcado = false;
                txtResultados.text = "G A M E    O V E R";
                ImagenLoser.SetActive(true);
                audioSource.Stop();
        }
    }

    bool BuscarLaLetra(char letra_buscar)
    {
        bool HayLetra = false;
        for (int i = 0; i < LaPalabra.Length; i++)
        {
            char letra_aux = LaPalabra[i];
            if (letra_buscar == letra_aux)
                HayLetra = true;
        }
        return HayLetra;
    }

    void ReemplazarLetra(char letra_reem)
    {
        char[] caracteres = PalabraMostrar.ToCharArray();
        for (int i = 0; i < LaPalabra.Length; i++)
        {
            char letra_aux = LaPalabra[i];
            if (letra_reem == letra_aux)
            {
                caracteres[i] = letra_reem;
                CantidadLetrasCorrectas += 1;
            }
        }
        PalabraMostrar = new string(caracteres);
    }

    GameObject BuscoImagen(int posicion)
    {
        string nombreobjeto;
        GameObject objeto;

        switch (posicion)
        {
            case 0:
                nombreobjeto = "imgAhorcado";
                break;
            case 1:
                nombreobjeto = "imgAhorcado_1";
                break;
            case 2:
                nombreobjeto = "imgAhorcado_2";
                break;
            case 3:
                nombreobjeto = "imgAhorcado_3";
                break;
            case 4:
                nombreobjeto = "imgAhorcado_4";
                break;
            case 5:
                nombreobjeto = "imgAhorcado_5";
                break;
            case 6:
                nombreobjeto = "imgAhorcado_6";
                break;
            case 7:
                nombreobjeto = "imgAhorcado_7";
                break;
            case 8:
                nombreobjeto = "imgAhorcado_8";
                break;
            default:
                nombreobjeto = "Image_9";
                break;
        }
        
        objeto = GameObject.Find(nombreobjeto);
        return objeto;
    }
    
    public void VolveralJuego()
    {
        SceneManager.LoadScene("House(Inside)");
    }
}
