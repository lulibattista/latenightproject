using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuCOntrol : MonoBehaviour
{
    public float TiempoOriginal;
    public float TiempoPasado;
    public float TiempoRestante;
    public int CantidadVeces = 1;
    public GameObject ImagenUno;
    public GameObject ImagenDos;
    public bool SiImagen;

    // Start is called before the first frame update
    void Start()
    {
        TiempoOriginal = 200;
        TiempoPasado = 0;
        TiempoRestante = TiempoOriginal;
        SiImagen = true;
        IniciaTemporizador();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

    }

    // Update is called once per frame
    void Update()
    {
        if (TiempoRestante <= 0)
        {
            if (SiImagen)
            {
                ImagenUno.SetActive(false);
                ImagenDos.SetActive(true);
                SiImagen = false;
                TiempoOriginal = 200;
                TiempoPasado = 0;
                TiempoRestante = TiempoOriginal;
                IniciaTemporizador();
            }
            else
            {
                ImagenDos.SetActive(false);
                ImagenUno.SetActive(true);
                SiImagen = true;
                TiempoOriginal = 200;
                TiempoPasado = 0;
                TiempoRestante = TiempoOriginal;
                IniciaTemporizador();
            }
        }
        else
        {
            IniciaTemporizador();
        }
    }
    public void Play()
    {
        SceneManager.LoadScene("Street");
    }
    public void Exit()
    {
        Application.Quit();
    }

    public void VolverMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private IEnumerator Esperar()
    {
        int tiempo_espera = 20;

        while (tiempo_espera > 0)
        {

            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
    }
    public void IniciaTemporizador()
    {
        StartCoroutine("Esperar");
        if (TiempoPasado <= TiempoOriginal)
        {
            StartCoroutine("Esperar");
            TiempoPasado += 1;
            TiempoRestante = TiempoOriginal - TiempoPasado;

        }
    }
}
