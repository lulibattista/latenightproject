using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class MemoControl : MonoBehaviour
{
    public TMP_Text txtLlamada;
    public TMP_Text txtcantidad;
    public TMP_Text txtIntentos;
    public int Llamada;
    public int TarjetasDisponibles;
    public int CantidadIntentos;
    public List<string> listaDeTarjetas;
    public GameObject PrimerTarjeta;
    public GameObject SegundaTarjeta;
    public int IndicePrimeraElejida;
    public int IndiceSegundaElegida;
    public bool Winner;
    public GameObject WinnerLogo;
    public GameObject LoserLogo;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        WinnerLogo.SetActive(false);
        LoserLogo.SetActive(false);
        Winner = false;
        Llamada = 0;
        TarjetasDisponibles = 16;
        listaDeTarjetas = new List<string>() { "RANA", "PEZ", "MARIPOSA", "HORMIGA", "MARIQUITA", "MARIQUITA", "CARACOL", "ABEJA", "RANA", "HORMIGA", "ABEJA", "PEZ", "MARIPOSA", "CARACOL", "MOSCA", "MOSCA" };
    }

    // Update is called once per frame
    void Update()
    {
        txtLlamada.text = "Vamos llamada = " + Llamada;
        txtcantidad.text = "Cantidad Tarjetas = " + TarjetasDisponibles;
        txtIntentos.text = "Cantidad Fallidos = " + CantidadIntentos;
        if(TarjetasDisponibles == 0)
        {
            Winner = true;
            
            WinnerLogo.SetActive(true);
            GameTimer.GaneMemotest = true;
            audioSource.Stop();
        }
        else if(CantidadIntentos == 7)
        {
            Winner = false;
            
            LoserLogo.SetActive(true);
            audioSource.Stop();
        }
    }
    public void VolveralJuego()
    {
        SceneManager.LoadScene("House(Inside)");
    }
}
