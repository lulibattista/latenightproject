using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MovementControl : MonoBehaviour
{
    Rigidbody miRB;
    public int rapidez;
    public bool EntraCasa = false;
    public bool Llaves = false;
    public GameObject Keys;
    public GameObject MissionText;
    public GameObject InteractionText;
    public GameObject InteractionText2;
    public GameObject InteractionText3;
    public bool canMove;
    //public CameraControl Camara;
    public bool PastillasCerca;
    public PilllsControl DesactivarPastillas;
    public SanityControl SanityMas;
    public bool EntrarMiniJuegos;
    public static bool VieneDeMiniJuegos = false;
    public static Vector3 PosicionPostMiniJuegos;
    public static Vector3 DireccionPostMiniJuegos;

    // Start is called before the first frame update
    void Start()
    {
        miRB = GetComponent<Rigidbody>();
        rapidez = 10;
        InteractionText.SetActive(false);
        canMove = true;
        PastillasCerca = false;
        EntrarMiniJuegos = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            
            float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidez;
            float movimientoCostados = Input.GetAxis("Horizontal") * rapidez;

            movimientoAdelanteAtras *= Time.deltaTime;
            movimientoCostados *= Time.deltaTime;

            miRB.transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
            if (VieneDeMiniJuegos)
            {
                VieneDeMiniJuegos = false;
                miRB.GetComponent<Transform>().position = PosicionPostMiniJuegos;
                miRB.GetComponent<Transform>().forward = DireccionPostMiniJuegos;
            }
            if (Input.GetKeyDown(KeyCode.G) && (EntraCasa == true))
            {
                MissionText.SetActive(false);
                
                SceneManager.LoadScene("House(Inside)");
            }
            if (Input.GetKeyDown(KeyCode.G) && (Llaves == true))
            {
                Keys.SetActive(true);
                MissionText.SetActive(false);
                InteractionText.SetActive(false);

            }
            if((PastillasCerca == true) && Input.GetKeyDown(KeyCode.G))
            {
                DesactivarPastillas.Desaparecer();
                SanityMas.PastillasSanity();
                PastillasCerca = false;
                InteractionText3.SetActive(false);
            }
            if((EntrarMiniJuegos ==true) && Input.GetKeyDown(KeyCode.G))
            {
                PosicionPostMiniJuegos = miRB.GetComponent<Transform>().position;
                DireccionPostMiniJuegos = miRB.GetComponent<Transform>().forward;
                InteractionText2.SetActive(false);
                SceneManager.LoadScene("MiniGame Menu");
                VieneDeMiniJuegos = true;
            }
        }
        else
        {
            miRB.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
        }


        
    }

    public void StopMovement()
    {
        canMove = false;
        
    }

    public void AllowMovement()
    {
        canMove = true;
        
    }


    void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("ChangeScene"))
        {
            
            EntraCasa = true;
            InteractionText.SetActive(true);


        }
        if (collision.gameObject.CompareTag("Keys"))
        {
            Llaves = true;
            InteractionText.SetActive(true);


        }
        if (collision.gameObject.CompareTag("Pastillas"))
        {
            if (DesactivarPastillas.Visibilidad)
            {
                PastillasCerca = true;
                InteractionText3.SetActive(true);
}
        }
        if(collision.gameObject.CompareTag("MiniGames"))
        {
            EntrarMiniJuegos = true;
            InteractionText2.SetActive(true);
            
        }
    }
    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("MiniGames"))
        {
            EntrarMiniJuegos = false;
            InteractionText2.SetActive(false);

        }
        if (collision.gameObject.CompareTag("Keys"))
        {
            Llaves = false;
            InteractionText.SetActive(false);


        }
        if (collision.gameObject.CompareTag("Pastillas"))
        {
            if (DesactivarPastillas.Visibilidad)
            {
                PastillasCerca = false;
                InteractionText3.SetActive(false);
            }
        }
    }
}
