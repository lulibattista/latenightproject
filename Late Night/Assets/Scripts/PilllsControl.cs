using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilllsControl : MonoBehaviour
{
    public GameObject Pastillas;
    public GameObject PastillasDos;
    public GameObject PastillasTres;
    public GameObject PastillasCuatro;
    public GameObject PastillasCinco;
    public bool Visibilidad;
    // Start is called before the first frame update
    void Start()
    {
        Visibilidad = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Visibilidad)
        {
            Pastillas.SetActive(true);
            PastillasDos.SetActive(true);
            PastillasTres.SetActive(true);
            PastillasCuatro.SetActive(true);
            PastillasCinco.SetActive(true);

        }
        else
        {
            Pastillas.SetActive(false);
            PastillasDos.SetActive(false);
            PastillasTres.SetActive(false);
            PastillasCuatro.SetActive(false);
            PastillasCinco.SetActive(false);
        }
    }
    public void Aparecer()
    {
        Visibilidad = true;
    }
    public void Desaparecer()
    {
        
        Visibilidad = false;
        
        
    }


}
