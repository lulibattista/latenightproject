using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Puzzle9Control : MonoBehaviour
{
    public List<int> PosicionesNumeros;
    public bool WinnerPuzzle9;
    public int PosicionNumeral;
    public GameObject LoserLogo;
    public GameObject WinnerLogo;
    public AudioSource audioSource;
    private bool ejecutarTemporizador = true;
    public  float TiempoOriginal;
    public  float TiempoPasado;
    public  float TiempoRestante;
    public TMP_Text TimeramText;
    public Image imagenTimer;
    

    void Start()
    {
        
        WinnerPuzzle9 = false;
        PosicionNumeral = 8;
        PosicionesNumeros = new List<int>() { 4, 6, 7, 1, 3, 5, 8, 9, 2 }; 
        TiempoOriginal = 55555;
        TiempoPasado = 0;
        TiempoRestante = TiempoOriginal;
    }

    // Update is called once per frame
    void Update()
    {
        if (TiempoRestante >= 0)
        {
            IniciaTemporizador();
            //TimeramText.text = "Tiempo= " + TiempoRestante;
            imagenTimer.fillAmount = (TiempoOriginal - TiempoPasado) / TiempoOriginal;
            WinnerPuzzle9 = NumerosEnOrder();
            if (WinnerPuzzle9)
            {
                
                DetenerTemporizador();
                WinnerLogo.SetActive(true);
                GameTimer.GanePuzzle9 = true;
                audioSource.Stop();
                
            }
        }
        else
        {
            DetenerTemporizador();
            LoserLogo.SetActive(true);
            audioSource.Stop();
        }


    }

    bool NumerosEnOrder()
    {
        bool Respuesta = true;
        for(int i = 0; i < PosicionesNumeros.Count; i++)
        {
            if (PosicionesNumeros[i] != i + 1)
            {
                Respuesta = false;
            }
        }
        return Respuesta;
    }
    public void IniciaTemporizador()
    {
        if (ejecutarTemporizador)
        {
            StartCoroutine("Esperar");
            if (TiempoPasado <= TiempoOriginal)
            {
                StartCoroutine("Esperar");
                TiempoPasado += 1;
                TiempoRestante = TiempoOriginal - TiempoPasado;

            }
        }

    }
    private IEnumerator Esperar()
    {
        int tiempo_espera = 20;

        while (tiempo_espera > 0)
        {

            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
    }

    public void DetenerTemporizador()
    {
        ejecutarTemporizador = false;

    }
    public void VolveralJuego()
    {
        SceneManager.LoadScene("House(Inside)");
    }
}
