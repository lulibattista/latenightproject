using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RaycastDesdeCamara : MonoBehaviour
{
    
    public string tagDelObjeto = "Ghost";
    public Camera camaraDelJugador;
    public SanityControl sanityControl;


    void Start()
    {
        
        if (camaraDelJugador == null)
        {
            camaraDelJugador = Camera.main;
        }
    }

    void Update()
    {
        
        Ray rayoDesdeCamara = camaraDelJugador.ScreenPointToRay(Input.mousePosition);

       
        RaycastHit hit;

        
        if (Physics.Raycast(rayoDesdeCamara, out hit) && hit.collider.CompareTag(tagDelObjeto))
        {
            sanityControl.IniciaTemporizador();
        }
    }
}
