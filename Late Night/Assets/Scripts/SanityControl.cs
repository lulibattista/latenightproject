using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SanityControl : MonoBehaviour
{
    public static float SanityOriginal;
    public static float SanityPasado;
    public static float SanityRestante;
    public Image BarraSanity;
    public GameManager ManagerGame;
    public GameTimer TiempoJuego;
    public PilllsControl ActivarPastillas;

    void Start()
    {
        SanityOriginal = 2000;
        SanityPasado = 0;
        SanityRestante = SanityOriginal;

        if (ManagerGame == null)
        {
            Debug.LogError("GameManager no est� asignado en SanityControl.");
        }
    }

    void Update()
    {
        BarraSanity.fillAmount = SanityRestante / SanityOriginal;
        if (SanityRestante <= 0)
        {
            if (ManagerGame != null)
            {
                ManagerGame.GameOver();
                TiempoJuego.DetenerTemporizador();
            }
        }
        else if(SanityRestante < (SanityOriginal / 2))
        {
            ActivarPastillas.Aparecer();
        }
    }

    private IEnumerator Esperar()
    {
        int tiempo_espera = 20;

        while (tiempo_espera > 0)
        {

            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
    }
    public void IniciaTemporizador()
    {
        StartCoroutine("Esperar");
        if(SanityPasado <= SanityOriginal)
        {
            StartCoroutine("Esperar");
            SanityPasado += 1;
            SanityRestante = SanityOriginal - SanityPasado;
            

        }
    }
    public void PastillasSanity()
    {
        SanityRestante = 1000;
    }

}
