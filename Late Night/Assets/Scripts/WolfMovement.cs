using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfMovement : MonoBehaviour
{
    public Vector3 posicionLivingRoom = new Vector3(9.7f, 0.5700002f, -1.4f);
    public float tiempoTotalLobo;
    public float tiempoPasadoLobo;
    public float tiempoRestanteLobo;
    public bool tiempoTerminadoLobo = false;
    public GameObject LoboGameObject;
    public GameObject LoboGameObject2;
    public GameObject LoboGameObject3;
    public GameObject LoboGameObject4;
    public int NumeroAleatorio;
    

    // Start is called before the first frame update
    void Start()
    {
        tiempoPasadoLobo = 0f;
        tiempoTotalLobo = 2000f;
        tiempoRestanteLobo = tiempoTotalLobo;

        
    }
    

    // Update is called once per frame
    void Update()
    {
        if (tiempoRestanteLobo == 0)
        {
            GenerarEnemigo();
        }
        else
        {
            IniciaTemporizadorLobo();
        }
    }

    public void GenerarEnemigo()
    {
        
        NumeroAleatorio = Random.Range(0, 4);
        tiempoPasadoLobo = 0f;
        LoboGameObject2.SetActive(false);
        LoboGameObject.SetActive(false);
        LoboGameObject3.SetActive(false);
        LoboGameObject4.SetActive(false);
        switch (NumeroAleatorio)
        {
            case 0:
                LoboGameObject.SetActive(true);
                IniciaTemporizadorLobo();
                break;
            case 1:
                LoboGameObject2.SetActive(true);
                IniciaTemporizadorLobo();
                break;
            case 2:
                LoboGameObject3.SetActive(true);
                IniciaTemporizadorLobo();
                break;
            case 3:
                LoboGameObject4.SetActive(true);
                IniciaTemporizadorLobo();
                break;
            default:
                LoboGameObject.SetActive(false);
                break;
        }
        
        
    }

    public void IniciaTemporizadorLobo()
    {
        StartCoroutine("Esperar");
        if (tiempoPasadoLobo <= tiempoTotalLobo)
        {
            StartCoroutine("Esperar");
            tiempoPasadoLobo += 1;
            tiempoRestanteLobo = tiempoTotalLobo - tiempoPasadoLobo;
            //Debug.Log("" + tiempoRestanteLobo);
        }
        else
        {
            tiempoTerminadoLobo = true;
        }
    }
    private IEnumerator Esperar()
    {
        int tiempo_espera = 20;

        while (tiempo_espera > 0)
        {

            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
    }

}
